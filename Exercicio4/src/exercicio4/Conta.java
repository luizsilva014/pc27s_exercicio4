package exercicio4;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Luiz
 */
public class Conta {
    private String numero;
    private String agencia;
    private static double saldo=0;

    public Conta(String numero, String agencia) {
        this.numero = numero;
        this.agencia = agencia;
    }

    public void sacar(double valor){
        if (this.saldo - valor >=0 ) {
            this.saldo = this.saldo - valor;
        }
    }
    
    public void depositar(double valor){
        this.saldo = this.saldo + valor;
    }

    public double getSaldo(){
        return this.saldo;
    }
}
