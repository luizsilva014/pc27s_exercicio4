/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio4;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author lwyz_
 */
public class Exercicio4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Conta conta = new Conta("99999-9", "888-9");
        
        conta.depositar(50);
        
        Movimentacao mov1 = new Movimentacao(conta);
        Movimentacao mov2 = new Movimentacao(conta);
        
        ExecutorService executor = Executors.newCachedThreadPool();
        
        executor.execute(mov1);
        executor.execute(mov2);
        
        //A partir daqui nao aceita mais threads
        executor.shutdown();
        
        //Mostra o conteudo do vetorCompartilhado apos as threads finalizarem
        try {
            //Espera 1 minuto para que todas as threads finalizem
            boolean tasksEnded = executor.awaitTermination(1, TimeUnit.MINUTES);
            
            if (tasksEnded)
                System.out.println(conta.getSaldo());
            else
                System.out.println("Timeout!");            
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }
    
}
