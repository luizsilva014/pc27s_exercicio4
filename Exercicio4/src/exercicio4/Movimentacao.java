/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio4;

import java.util.Random;

/**
 *
 * @author Luiz
 */
public class Movimentacao implements Runnable{

    private Conta conta;
    private static Random generator = new Random();

    public Movimentacao(Conta conta) {
        this.conta = conta;
    }
        
    @Override
    public synchronized void run() {
        try {
            //Coloca a thread para dormir entre 0 e 500ms
            Thread.sleep(generator.nextInt(5000));
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        conta.sacar(50);
    }
    
}
